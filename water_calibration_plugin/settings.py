#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
from pyforms import conf

WATER_PLUGIN_ICON = os.path.join( os.path.dirname(__file__), 'resources', 'water.png' )

WATER_PLUGIN_WINDOW_SIZE = 900, 500
WATER_PLUGIN_HISTORY_WINDOW_SIZE = 800, 300

# Names of the files
WATER_PLUGIN_NAME_EXPERIMENT = 'WaterCalibration'
WATER_PLUGIN_NAME_SETUPS     = 'setCalibration'
WATER_PLUGIN_NAME_TASK       = 'water_calibration_task'
WATER_PLUGIN_PATH_TASK       = os.path.join(os.path.dirname(__file__), "water_calibration_task.py")

WATER_PLUGIN_PATH_DEFAULT    = conf.GENERIC_EDITOR_PLUGINS_PATH + "/water-calibration-plugin/default_values.npy"
WATER_PLUGIN_PATH_DATA = conf.GENERIC_EDITOR_PLUGINS_PATH + '/water-calibration-plugin/DATA'
if not os.path.exists(WATER_PLUGIN_PATH_DATA): # if DATA does not exist, create it
    os.makedirs(WATER_PLUGIN_PATH_DATA)

# Name files
WATER_PLUGIN_FILE_PATH  = WATER_PLUGIN_PATH_DATA + '/water_calibration_hystory.npy'


# SETTINGS PARAMETERS
TOT_PORTS = 8
GR_TO_MICROL = 1000  # convert gr --> ml --> micro l

MAX_VAL = 5
MIN_VAL = 0.0001
